import React, {Component} from 'react';
import {ThemeProvider} from 'react-native-elements';

import App from './App';

/*
• Perzonalizar tamaño de la fuente
Pareciera que el tamaño del texto se tiene que editar respecto a cada componente, (si se aplica a Text, quizá dejen de funcionar la funcionalidad de los h1, h2, h3, etc..)

• Colores de botones
Ya está listo, se dirige por el primary color

• Colores de fondo de la app
I doubt this is doable with RNE theming (Confirmed)
*/

const theme = {
  Button: {
    raised: true,
    /* textStyle: {fontSize: 30}, */
  },
  Avatar: {
    rounded: true,
  },
  Text: {
    /* textStyle: {fontSize: 16}, */
  },
  colors: {
    primary: '#0277bd', // este es el color que se toma para los botones
    secondary: '#CACF85', // sacado de https://coolors.co/0277bd-264653-cacf85-3e92cc-13293d
    searchBg: '#ffddc1',
  },

  View: {
    backgroundColor: '#004ba0',
  },
};

/* Aquí se exporta nuestro tema */
class Setup extends Component {
  render() {
    return (
      <ThemeProvider theme={theme}>
        <App />
      </ThemeProvider>
    );
  }
}

export default Setup;
