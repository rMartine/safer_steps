import React, {Component} from 'react';
import {View, Image, Alert} from 'react-native';
import {Text, Card} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';

class StairCard extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    console.log('Will staircard');
  }

  componentDidMount() {
    console.log('Did staircard');
  }

  render() {
    return (
      <View
        style={{
          minHeight: 90,
          maxHeight: 180,
          elevation: 4,
          marginTop: 8,
          marginBottom: 8,
          backgroundColor: 'white',
          borderRadius: 2,
        }}>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          {/* Image container */}
          <View style={{flex: 3}}>
            <Image
              style={{width: '100%', height: '100%'}}
              source={{
                uri: this.props.img,
              }}
              resizeMode="cover"
            />
          </View>

          {/* Alias container */}
          <View style={{flex: 7, paddingLeft: 24}}>
            <Text>{this.props.title} </Text>
          </View>

          {/* Button container */}
          <View style={{flex: 2}}>
            <Icon
              raised
              name="edit"
              type="font-awesome"
              color="#f50"
              onPress={() => this.props.onPress}
            />
          </View>
        </View>
      </View>
    );
  }
}

export default StairCard;
