import React from 'react';
import {Button, Text, View} from 'react-native';
import {Icon} from 'react-native-elements';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createBottomTabNavigator} from 'react-navigation-tabs';

import Home from './components/Home'
import Questionnaire from './components/Questionnaire'

const RootStack = createStackNavigator(
  {
    Home : { screen : Home},
    Questionnaire : { screen : Questionnaire }
  },
  {
    initialRouteName : 'Home',
    navigationOptions : {
      header: null
    }
  }
);

export default createAppContainer(RootStack);
