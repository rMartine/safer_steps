import React, {Component} from 'react';
import {View, ScrollView, Text, TouchableOpacity, StyleSheet} from 'react-native';
import {Button} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import Camera from './Camera';

const questionnaire = require('../questionnaire_photo.json');
class QuestionnaireView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      questionN: 'q1',
      answers: [],
    };
  }

  getIndexOfAnswer = questionN => {
    const {answers} = this.state;
    return answers.findIndex(x => x.alias === questionN) || false;
  };

  handleAnswerClick = (question, answer, value) => {
    const {answers} = this.state;
    // const questionAnswered = this.getIndexOfAnswer(question.alias);
    // if (questionAnswered) {
    //   console.log(questionAnswered);
    // }
    const newAnswer = {
      alias: question.alias,
      type: question.type,
      value: value ? value : answer.alias,
    };
    answers.push(newAnswer);
    this.setState({questionN: answer.nextq});
  };

  findQuestion = questionN => {
    return questionnaire.questionnaire.questions.find(
      x => x.alias === questionN,
    );
  };

  renderCameraQuestion = (qtext, nextq) => {
    return (
      <View>
        <Camera />
      </View>
    );
  };

  renderQuestion = () => {
    const {questionN} = this.state;
    const question = this.findQuestion(questionN);
    const answers = question.answers;
    /* Aquí se llama dependiendo el tipo */
    /* Se llamarían componentes <OptionComponent text answers onClick /> */
    return this.componentExample(question, answers, this.handleAnswerClick);
  };

  componentExample = (question, answers, onClickF) => {
    const qtext = question.qtext;
    return (
      <View>
        <Text>{qtext}</Text>
        {answers.map((answer, i) => (
          <Button
            key={i}
            title={answer.atext}
            onPress={() => onClickF(question, answer, false)}
          />
        ))}
      </View>
    );
  };

  nextQuestion = () => {
    const {answers, questionN} = this.state;
    const answerIndex = this.getIndexOfAnswer(questionN);
    if (answerIndex && answerIndex < answers.length - 1) {
      this.setState({questionN: answers[answerIndex].alias});
    }
  };

  render() {
    return (
      <View style={{flex:1}}>
        <ScrollView>
          { this.renderCameraQuestion(questionnaire.questionnaire.questions[0].qtext, questionnaire.questionnaire.questions[0].nextq) }
        </ScrollView>
        <View style={{flexDirection: 'row'}}>
          <Button title="Prev" />
          <Button title="Cancel" />
          <Button title="Next" />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black',
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20,
  },
});

export default QuestionnaireView;
