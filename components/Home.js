import React, {Component} from 'react';
import {View} from 'react-native';
import {Button} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import styles from '../styles';
import StairCard from './StairCard.js';
import BodyText from './BodyText.js';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      stairs: [],
    };
    this.helpers = require('../helpers.js');
  }

  componentDidMount() {
    /*  this.helpers.asyncStorageGetItem('stairs', response => {
      this.setState({stairs: response});
    }); */
    this.setState({
      stairs: [
        {
          uuid: 1,
          alias: 'Basement',
          img:
            'https://s3.amazonaws.com/treehouse-content/uploads/photo_gallery/large/15761-pc120002.jpg',
        },
        {
          uuid: 2,
          alias: 'Second floor',
          img:
            'https://www.viewrail.com/wp-content/uploads/2018/10/Cost-hero-1024x683.jpg',
        },
      ],
    });
  }

  renderStairList() {
    if (this.state.stairs) {
      let object;

      console.log('Rendering list');
      this.state.stairs.forEach(item => {
        object += (
          <StairCard
            key={item.uuid}
            title={item.alias}
            onPress={() =>
              this.props.navigation.navigate('Questionnaire', {
                stairid: item.uuid,
              })
            }
            img={item.img}
          />
        );
      });

      return object;
    } else {
      return <BodyText value="There are no stairs captured yet." />;
    }
  }

  render() {
    const uuidv4 = require('uuid/v4');
    return (
      <View
        style={{
          flex: 1,
          flexDirection: this.state.stairs ? 'column' : 'column-reverse',
          justifyContent: this.state.stairs ? 'flex-start' : 'center',
          padding: 16,
          paddingTop: 24,
        }}>
        <Button
          /*  icon={<Icon name="arrow-right" size={15} color="white" />} */
          title="Add new stair"
          onPress={() =>
            this.props.navigation.navigate('Questionnaire', {
              stairid: uuidv4(),
            })
          }
        />
        {console.log(this.state.stairs)}
        {this.state.stairs != [] ? (
          this.state.stairs.map(item => {
            return (
              <StairCard
                key={item.uuid}
                title={item.alias}
                onPress={() =>
                  this.props.navigation.navigate('Questionnaire', {
                    stairid: item.uuid,
                  })
                }
                img={item.img}
              />
            );
          })
        ) : (
          <BodyText value="There are no stairs captured yet." />
        )}
      </View>
    );
  }
}

export default Home;
